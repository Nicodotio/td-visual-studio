﻿using System;

namespace Exercice_1
{
    class Program
    {
        static void Main()
        {
            int first, second;
            bool success;
            do
            {
                Console.WriteLine("Saisissez le premier nombre :");
                success = int.TryParse(Console.ReadLine(), out first);
                if (!success)
                {
                    Console.WriteLine("Saisie invalide");
                }
            } while (!success);
            do
            {
                Console.WriteLine("Saisissez le deuxième nombre :");
                success = int.TryParse(Console.ReadLine(), out second);
                if (!success)
                {
                    Console.WriteLine("Saisie invalide");
                }
            } while (!success);

            Console.WriteLine(Addition(first, second));
        }

        static int Addition(int first, int second)
        {
            return first + second;
        }
    }
}
