﻿using System;

namespace Exercice_5
{
    class Program
    {
        static void Main()
        {
            bool success;
            int numberOfTry = 0;
            int inputNumber;
            int randomNumber = new Random().Next(1000);

            do
            {
                do
                {
                    Console.Write("Tapez un prix : ");
                    success = int.TryParse(Console.ReadLine(), out inputNumber);
                    if (!success)
                    {
                        Console.WriteLine("Saisie invalide");
                    }
                } while (!success);
                numberOfTry++;
                if (inputNumber < randomNumber)
                {
                    Console.WriteLine("Trop petit");
                }
                else if (inputNumber > randomNumber)
                {
                    Console.WriteLine("Trop grand");
                }
                else
                {
                    Console.WriteLine($"BRAVO ! Vous avez gagné en {numberOfTry} essai(s)");
                }
            } while (randomNumber != inputNumber);
        }
    }
}
