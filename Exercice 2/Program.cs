﻿using System;

namespace Exercice_2
{
    class Program
    {
        static void Main()
        {
            float fahrenheitTemp;
            bool success;
            do
            {
                Console.Write("Une température en Fahrenheit : ");
                success = float.TryParse(Console.ReadLine(), out fahrenheitTemp);
                if (!success)
                {
                    Console.WriteLine("Saisie invalide");
                }
            } while (!success);
            double celsiusTemp = Math.Round(5.0 / 9.0 * (fahrenheitTemp - 32.0), 1);

            Console.WriteLine($"équivaut à {celsiusTemp} degrés Celsius.");
        }
    }
}
