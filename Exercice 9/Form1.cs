﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Exercice_9
{
    public partial class Form1 : Form
    {

        private int thickness = 5;
        private Pen pen;


        private readonly Graphics canvas;
        public Form1()
        {
            InitializeComponent();
            canvas = pictureBox.CreateGraphics();
            pen = new Pen(Color.Black, thickness);
        }

        private void QuitterStripMenuItem1_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void CouleursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorDialog.ShowDialog();
            pen.Color = colorDialog.Color;
        }

        private void PlusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            thickness += 5;
            pen.Width = thickness;
        }

        private void MoinsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            thickness -= 5;
            pen.Width = thickness;
        }

        private void AProposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Créé par Nicolas FONTAINE", "À propos");
        }

        private void PictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                canvas.DrawRectangle(pen, new Rectangle(e.X, e.Y, thickness, thickness));
            }
        }
    }
}
