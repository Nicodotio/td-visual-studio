﻿namespace Exercice_6
{
    public class Prix
    {
        private double Tva { get; set; }
        public double PrixHT { get ; set; }

        public Prix(double tva, double prixHT)
        {
            Tva = tva;
            PrixHT = prixHT;
        }

        public double CalculValeurTVA()
        {
            return PrixHT * Tva/100;
        }

        public double CalculPrixTTC()
        {
            return PrixHT + CalculValeurTVA();
        }
    }
}
