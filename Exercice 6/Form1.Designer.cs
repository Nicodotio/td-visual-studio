﻿
namespace Exercice_6
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNom = new System.Windows.Forms.Label();
            this.labelPrixHT = new System.Windows.Forms.Label();
            this.labelTVA = new System.Windows.Forms.Label();
            this.buttonCalcul = new System.Windows.Forms.Button();
            this.numericUpDownPrixHT = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownTVA = new System.Windows.Forms.NumericUpDown();
            this.textBoxNom = new System.Windows.Forms.TextBox();
            this.labelTag = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPrixHT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTVA)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNom
            // 
            this.labelNom.AutoSize = true;
            this.labelNom.Location = new System.Drawing.Point(61, 27);
            this.labelNom.Name = "labelNom";
            this.labelNom.Size = new System.Drawing.Size(45, 17);
            this.labelNom.TabIndex = 0;
            this.labelNom.Text = "Nom :";
            // 
            // labelPrixHT
            // 
            this.labelPrixHT.AutoSize = true;
            this.labelPrixHT.Location = new System.Drawing.Point(44, 64);
            this.labelPrixHT.Name = "labelPrixHT";
            this.labelPrixHT.Size = new System.Drawing.Size(62, 17);
            this.labelPrixHT.TabIndex = 1;
            this.labelPrixHT.Text = "Prix HT :";
            // 
            // labelTVA
            // 
            this.labelTVA.AutoSize = true;
            this.labelTVA.Location = new System.Drawing.Point(63, 99);
            this.labelTVA.Name = "labelTVA";
            this.labelTVA.Size = new System.Drawing.Size(43, 17);
            this.labelTVA.TabIndex = 1;
            this.labelTVA.Text = "TVA :";
            // 
            // buttonCalcul
            // 
            this.buttonCalcul.Location = new System.Drawing.Point(47, 133);
            this.buttonCalcul.Name = "buttonCalcul";
            this.buttonCalcul.Size = new System.Drawing.Size(207, 23);
            this.buttonCalcul.TabIndex = 3;
            this.buttonCalcul.Text = "Créer l\'étiquette";
            this.buttonCalcul.UseVisualStyleBackColor = true;
            this.buttonCalcul.Click += new System.EventHandler(this.ButtonCompute_Click);
            // 
            // numericUpDownPrixHT
            // 
            this.numericUpDownPrixHT.DecimalPlaces = 2;
            this.numericUpDownPrixHT.Location = new System.Drawing.Point(134, 62);
            this.numericUpDownPrixHT.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownPrixHT.Name = "numericUpDownPrixHT";
            this.numericUpDownPrixHT.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownPrixHT.TabIndex = 1;
            // 
            // numericUpDownTVA
            // 
            this.numericUpDownTVA.DecimalPlaces = 1;
            this.numericUpDownTVA.Location = new System.Drawing.Point(134, 97);
            this.numericUpDownTVA.Name = "numericUpDownTVA";
            this.numericUpDownTVA.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownTVA.TabIndex = 2;
            // 
            // textBoxNom
            // 
            this.textBoxNom.Location = new System.Drawing.Point(134, 21);
            this.textBoxNom.Name = "textBoxNom";
            this.textBoxNom.Size = new System.Drawing.Size(120, 22);
            this.textBoxNom.TabIndex = 0;
            // 
            // labelTag
            // 
            this.labelTag.AutoSize = true;
            this.labelTag.Location = new System.Drawing.Point(44, 170);
            this.labelTag.Name = "labelTag";
            this.labelTag.Size = new System.Drawing.Size(0, 17);
            this.labelTag.TabIndex = 5;
            // 
            // Form1
            // 
            this.AcceptButton = this.buttonCalcul;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 255);
            this.Controls.Add(this.labelTag);
            this.Controls.Add(this.textBoxNom);
            this.Controls.Add(this.numericUpDownTVA);
            this.Controls.Add(this.numericUpDownPrixHT);
            this.Controls.Add(this.buttonCalcul);
            this.Controls.Add(this.labelTVA);
            this.Controls.Add(this.labelPrixHT);
            this.Controls.Add(this.labelNom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Calcul prix";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPrixHT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTVA)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNom;
        private System.Windows.Forms.Label labelPrixHT;
        private System.Windows.Forms.Label labelTVA;
        private System.Windows.Forms.Button buttonCalcul;
        private System.Windows.Forms.NumericUpDown numericUpDownPrixHT;
        private System.Windows.Forms.NumericUpDown numericUpDownTVA;
        private System.Windows.Forms.TextBox textBoxNom;
        private System.Windows.Forms.Label labelTag;
    }
}

