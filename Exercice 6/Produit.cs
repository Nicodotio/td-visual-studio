﻿namespace Exercice_6
{
    public class Produit
    {
        public string Nom { get; set; }
        public Prix Prix { get; set; }

        public Produit(string nom, double tva, double prixHT)
        {
            Nom = nom;
            Prix = new Prix(tva, prixHT);
        }

        public override string ToString()
        {
            return $"Nom : {Nom}\nPrix HT : {Prix.PrixHT}\nTVA : {Prix.CalculValeurTVA()}\nPrix TTC : {Prix.CalculPrixTTC()}";
        }
    }
}
