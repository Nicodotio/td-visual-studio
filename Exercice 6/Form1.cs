﻿using System;
using System.Windows.Forms;

namespace Exercice_6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ButtonCompute_Click(object sender, EventArgs e)
        {
            Produit produit = new Produit(textBoxNom.Text, decimal.ToDouble(numericUpDownTVA.Value), decimal.ToDouble(numericUpDownPrixHT.Value));
            labelTag.Text = produit.ToString();
        }
    }
}
