﻿using System;
using System.Collections.Generic;

namespace Exercice_4
{
    class Program
    {
        static void Main()
        {
            List<int> numberList = new();
            bool success;
            for (int i = 1; i <= 3; i++)
            {
                do
                {
                    if (i == 1)
                    {
                        Console.Write("1er nombre : ");
                    }
                    else
                    {
                        Console.Write($"{i}ème nombre : ");
                    }

                    success = int.TryParse(Console.ReadLine(), out int number);
                    if (!success)
                    {
                        Console.WriteLine("Saisie invalide");
                    }
                    else
                    {
                        numberList.Add(number);
                    }
                } while (!success);
            }
            numberList.Sort();
            Console.Write("Les nombres dans l'ordre croissant : ");
            foreach (int nombre in numberList)
            {
                Console.Write($" {nombre}");
            }
        }
    }
}
