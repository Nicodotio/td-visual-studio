﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace Exercice_7
{
    public class ScoreJoueur
    {
        public string Nom { get; private set; }
        public int NbPoints { get; set; }

        public ScoreJoueur(string nom, int nbPoints)
        {
            Nom = nom;
            NbPoints = nbPoints;
        }

        public void Ajouter(int points)
        {
            NbPoints+=points;
        }
    }
}
