﻿using System;
using System.Windows.Forms;

namespace Exercice_7
{
    public partial class Form1 : Form
    {

        private SuiviDeMatch suiviDeMatch;
        public Form1()
        {
            InitializeComponent();
        }

        private void ButtonInit_Click(object sender, EventArgs e)
        {
            suiviDeMatch = new SuiviDeMatch(textBoxNomJoueur1.Text, textBoxNomJoueur2.Text, decimal.ToInt32(numericUpDownIncrement.Value));
            buttonJoueur1.Text = textBoxNomJoueur1.Text;
            buttonJoueur2.Text = textBoxNomJoueur2.Text;
            buttonJoueur1.Show();
            buttonJoueur2.Show();
            textBoxMessages.Clear();
        }

        private void ButtonJoueur1_Click(object sender, EventArgs e)
        {
            textBoxMessages.AppendText($"{suiviDeMatch.AugmenterJoueur(1)}{Environment.NewLine}");
        }

        private void ButtonJoueur2_Click(object sender, EventArgs e)
        {
            textBoxMessages.AppendText($"{suiviDeMatch.AugmenterJoueur(2)}{Environment.NewLine}");
        }
    }
}
