﻿namespace Exercice_7
{
    public class SuiviDeMatch
    {
        public ScoreJoueur ScoreJoueur1 { get; private set; }
        public ScoreJoueur ScoreJoueur2 { get; private set; }
        public int Increment { get; private set; }

        public SuiviDeMatch(string nomJoueur1, string nomJoueur2, int increment)
        {
            ScoreJoueur1 = new ScoreJoueur(nomJoueur1, 0);
            ScoreJoueur2 = new ScoreJoueur(nomJoueur2, 0);
            Increment = increment;
        }

        public void AugmenterJoueur1()
        {
            ScoreJoueur1.NbPoints++;
        }

        public void AugmenterJoueur2()
        {
            ScoreJoueur2.NbPoints++;
        }

        public string AugmenterJoueur(int numeroJoueur)
        {
            if (numeroJoueur == 1)
            {
                AugmenterJoueur1();
            }
            else if (numeroJoueur == 2)
            {
                AugmenterJoueur2();
            }

            if (ScoreJoueur1.NbPoints > ScoreJoueur2.NbPoints)
            {
                return $"{ScoreJoueur1.Nom} mène devant {ScoreJoueur2.Nom}";
            }
            else if (ScoreJoueur1.NbPoints < ScoreJoueur2.NbPoints)
            {
                return $"{ScoreJoueur2.Nom} mène devant {ScoreJoueur1.Nom}";
            }
            else
            {
                return $"{ScoreJoueur1.Nom} à égalité avec {ScoreJoueur2.Nom}";
            }
        }
    }
}
