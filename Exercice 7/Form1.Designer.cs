﻿
namespace Exercice_7
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelIncrement = new System.Windows.Forms.Label();
            this.labelNomJoueur1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownIncrement = new System.Windows.Forms.NumericUpDown();
            this.textBoxNomJoueur1 = new System.Windows.Forms.TextBox();
            this.textBoxNomJoueur2 = new System.Windows.Forms.TextBox();
            this.buttonInit = new System.Windows.Forms.Button();
            this.textBoxMessages = new System.Windows.Forms.TextBox();
            this.buttonJoueur1 = new System.Windows.Forms.Button();
            this.buttonJoueur2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncrement)).BeginInit();
            this.SuspendLayout();
            // 
            // labelIncrement
            // 
            this.labelIncrement.AutoSize = true;
            this.labelIncrement.Location = new System.Drawing.Point(123, 60);
            this.labelIncrement.Name = "labelIncrement";
            this.labelIncrement.Size = new System.Drawing.Size(78, 17);
            this.labelIncrement.TabIndex = 0;
            this.labelIncrement.Text = "Incrément :";
            // 
            // labelNomJoueur1
            // 
            this.labelNomJoueur1.AutoSize = true;
            this.labelNomJoueur1.Location = new System.Drawing.Point(96, 107);
            this.labelNomJoueur1.Name = "labelNomJoueur1";
            this.labelNomJoueur1.Size = new System.Drawing.Size(105, 17);
            this.labelNomJoueur1.TabIndex = 0;
            this.labelNomJoueur1.Text = " Nom joueur 1 :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(100, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nom joueur 2 :";
            // 
            // numericUpDownIncrement
            // 
            this.numericUpDownIncrement.Location = new System.Drawing.Point(223, 60);
            this.numericUpDownIncrement.Name = "numericUpDownIncrement";
            this.numericUpDownIncrement.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownIncrement.TabIndex = 0;
            // 
            // textBoxNomJoueur1
            // 
            this.textBoxNomJoueur1.Location = new System.Drawing.Point(223, 107);
            this.textBoxNomJoueur1.Name = "textBoxNomJoueur1";
            this.textBoxNomJoueur1.Size = new System.Drawing.Size(120, 22);
            this.textBoxNomJoueur1.TabIndex = 1;
            // 
            // textBoxNomJoueur2
            // 
            this.textBoxNomJoueur2.Location = new System.Drawing.Point(223, 146);
            this.textBoxNomJoueur2.Name = "textBoxNomJoueur2";
            this.textBoxNomJoueur2.Size = new System.Drawing.Size(120, 22);
            this.textBoxNomJoueur2.TabIndex = 2;
            // 
            // buttonInit
            // 
            this.buttonInit.Location = new System.Drawing.Point(223, 188);
            this.buttonInit.Name = "buttonInit";
            this.buttonInit.Size = new System.Drawing.Size(120, 23);
            this.buttonInit.TabIndex = 3;
            this.buttonInit.Text = "Initialisation";
            this.buttonInit.UseVisualStyleBackColor = true;
            this.buttonInit.Click += new System.EventHandler(this.ButtonInit_Click);
            // 
            // textBoxMessages
            // 
            this.textBoxMessages.Location = new System.Drawing.Point(99, 273);
            this.textBoxMessages.Multiline = true;
            this.textBoxMessages.Name = "textBoxMessages";
            this.textBoxMessages.ReadOnly = true;
            this.textBoxMessages.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxMessages.Size = new System.Drawing.Size(265, 141);
            this.textBoxMessages.TabIndex = 6;
            // 
            // buttonJoueur1
            // 
            this.buttonJoueur1.Location = new System.Drawing.Point(99, 244);
            this.buttonJoueur1.Name = "buttonJoueur1";
            this.buttonJoueur1.Size = new System.Drawing.Size(102, 23);
            this.buttonJoueur1.TabIndex = 4;
            this.buttonJoueur1.UseVisualStyleBackColor = true;
            this.buttonJoueur1.Visible = false;
            this.buttonJoueur1.Click += new System.EventHandler(this.ButtonJoueur1_Click);
            // 
            // buttonJoueur2
            // 
            this.buttonJoueur2.Location = new System.Drawing.Point(263, 244);
            this.buttonJoueur2.Name = "buttonJoueur2";
            this.buttonJoueur2.Size = new System.Drawing.Size(101, 23);
            this.buttonJoueur2.TabIndex = 5;
            this.buttonJoueur2.UseVisualStyleBackColor = true;
            this.buttonJoueur2.Visible = false;
            this.buttonJoueur2.Click += new System.EventHandler(this.ButtonJoueur2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 450);
            this.Controls.Add(this.textBoxMessages);
            this.Controls.Add(this.buttonJoueur2);
            this.Controls.Add(this.buttonJoueur1);
            this.Controls.Add(this.buttonInit);
            this.Controls.Add(this.textBoxNomJoueur2);
            this.Controls.Add(this.textBoxNomJoueur1);
            this.Controls.Add(this.numericUpDownIncrement);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelNomJoueur1);
            this.Controls.Add(this.labelIncrement);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Scores";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownIncrement)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelIncrement;
        private System.Windows.Forms.Label labelNomJoueur1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownIncrement;
        private System.Windows.Forms.TextBox textBoxNomJoueur1;
        private System.Windows.Forms.TextBox textBoxNomJoueur2;
        private System.Windows.Forms.Button buttonInit;
        private System.Windows.Forms.TextBox textBoxMessages;
        private System.Windows.Forms.Button buttonJoueur1;
        private System.Windows.Forms.Button buttonJoueur2;
    }
}

