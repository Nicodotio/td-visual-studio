﻿using System;

namespace Exercice_3
{
    class Program
    {
        static void Main()
        {
            int secondsInput;
            bool success;
            do
            {
                Console.Write("Une durée en secondes : ");
                success = int.TryParse(Console.ReadLine(), out secondsInput);
                if (!success || secondsInput < 0)
                {
                    Console.WriteLine("Saisie invalide");
                }
            } while (!success || secondsInput < 0);

            //int days = secondsInput / 86400;
            //int hours = (secondsInput % 86400) / 3600;
            //int minutes = ((secondsInput % 86400) % 3600) / 60;
            //int seconds = (((secondsInput % 86400) % 3600) % 60);

            //Console.WriteLine($"équivaut à {days} jour(s) {hours} heure(s) {minutes} minute(s) et {seconds} seconde(s).");

            TimeSpan timeSpan = TimeSpan.FromSeconds(secondsInput);

            Console.WriteLine($"équivaut à {timeSpan.Days} jour(s) {timeSpan.Hours} heure(s) {timeSpan.Minutes} minute(s) et {timeSpan.Seconds} seconde(s).");
        }
    }
}
