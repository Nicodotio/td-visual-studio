﻿using System;
using System.Windows.Forms;

namespace Exercice_5_Visuel
{
    public partial class Form1 : Form
    {
        int numberOfTry;
        int randomNumber;
        int min, max;
        Random aleaNumber = new Random();
        int inputNumber;
        public Form1()
        {
            InitializeComponent();
            Init();
        }

        private void ButtonManual_Click(object sender, EventArgs e)
        {
            numberOfTry++;
            inputNumber = decimal.ToInt32(numericUpDown.Value);
            if (inputNumber < randomNumber)
            {
                textBoxMessages.AppendText($"Trop petit{Environment.NewLine}");
            }
            else if (inputNumber > randomNumber)
            {
                textBoxMessages.AppendText($"Trop grand{Environment.NewLine}");
            }
            else
            {
                textBoxMessages.AppendText($"BRAVO ! Vous avez gagné en {numberOfTry} essai(s){Environment.NewLine}");
            }
        }


        private void ButtonInit_Click(object sender, EventArgs e)
        {
            Init();
        }

        private void ButtonAuto_Click(object sender, EventArgs e)
        {
            min = 0;
            max = 1000;
            inputNumber = max / 2;

            do
            {
                numberOfTry++;
                if (inputNumber < randomNumber)
                {
                    min = inputNumber;
                }
                else if (inputNumber > randomNumber)
                {
                    max = inputNumber;
                }
                inputNumber = (max - min) / 2 + min;
            } while (randomNumber != inputNumber);
            textBoxMessages.AppendText($"Prix : {randomNumber}, nombre d'essais : {numberOfTry}{Environment.NewLine}");
        }

        private void Init()
        {
            numberOfTry = 0;
            randomNumber = aleaNumber.Next(1000);
            numericUpDown.ResetText();
            textBoxMessages.ResetText();
        }
    }
}
