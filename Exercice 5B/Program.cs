﻿using System;

namespace Exercice_5B
{
    class Program
    {
        static void Main()
        {
            int numberOfTry;
            int randomNumber;
            int min, max;
            Random aleaNumber = new();
            int inputNumber;
            for (int i = 1; i <= 20; i++)
            {
                numberOfTry = 0;
                randomNumber = aleaNumber.Next(1000);
                min = 0;
                max = 1000;
                inputNumber = max / 2;

                do
                {
                    numberOfTry++;
                    if (inputNumber < randomNumber)
                    {
                        //Console.WriteLine("Trop petit");
                        min = inputNumber;
                    }
                    else if (inputNumber > randomNumber)
                    {
                        //Console.WriteLine("Trop grand");
                        max = inputNumber;
                    }
                    inputNumber = (max - min) / 2 + min;
                } while (randomNumber != inputNumber);
                Console.WriteLine($"Prix : {randomNumber}, nombre d'essais : {numberOfTry}");
            }
        }
    }
}
